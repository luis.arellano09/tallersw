/**
 * @file CondicionalesExercise12.cpp
 * @author Edgar Quiones (edgar06022002@gmail.com)
 * @brief exercise 12:
 		12.- Calcular la utilidad que un trabajador recibe en el reparto anual de utilidades si este se le asigna como un porcentaje de su salario mensual que depende de su antigedad en la empresa de acuerdo con la sig. tabla:
		Tiempo						Utilidad
	Menos de 1 ao						5 % del salario
	1 ao o mas y menos de 2 aos				7% del salario
	2 aos o mas y menos de 5 aos				10% del salario
	5 aos o mas y menos de 10 aos				15% del salario
	10 aos o mas							20% del salario

 * @version 1.0
 * @date 08.02.2022
 * 
 */
/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int globalParamSalaryRange1 = 12;
int globalParamSalaryRange2 = 24;
int globalParamSalaryRange3 = 60;
int globalParamSalaryRange4 = 150;

int globalParamSalaryUtilityRange1 = 0.06;

int monthsInput = 0;
float salaryMonthly = 0;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/

void Run();
void CollectData();
void CheckAccess();
float CalculatedUtility(int months, float salary);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
		
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
}
//=====================================================================================================

void CollectData(){
	
	cout<<"============Insert data============\r\n";
	
	cout<<"Enter you months working: ";
	cin>>monthsInput;
	cout<<"Enter you salary monthly: ";
	cin>>salaryMonthly;
}
//=====================================================================================================

void Calculate(){
	float utility = 0.0;
	if (utility=calculatedUtility(monthsInput,salaryMonthly) ) {
		cout<<"The utility is:"<<utility<<"\r\n";
	}	
}
//=====================================================================================================

float CalculatedUtility(int months, float salary){
	if ( globalSalaryRange1 >= months) {
		return salary*(globalParamSalaryUtilityRange1);		
	}else if ( globalVariable1 < months && months <= globalVariable2) {
		return salary*(0.07);
	}else if ( variable2 < months && months <= variable3) {
		return salary*(0.1);	
	}else if ( variable3 < months && months <= variable4) {
		return salary*(0.15);	
	}else if ( variable4 < months ) {
		return salary*(0.2);	
	}				
}

//=====================================================================================================


