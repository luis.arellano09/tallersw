/**
 * @file Exercise 08.cpp
 * @author Oscar Lopez (oscar.lopez10@unmsm.edu.pe)
 * @brief Una empresa registra el sexo, edad y estado civil de sus empleados a trav�s de un n�mero entero positivo de cuatro cifras de 
 		  acuerdo a lo siguiente: la primera cifra de la izquierda representa el estado civil (1 para soltero, 2 para casado, 3 para viudo 
		  y 4 para divorciado), las siguientes dos cifras representan la edad y la tercera cifra representa el sexo (1 para femenino y 2 para masculino). 
		  Dise�e un programa que determine el estado civil, edad y sexo de un empleado conociendo el n�mero que empaqueta dicha informaci�n.
 * @version 1.8
 * @date 11.02.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <windows.h>
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES 
 *******************************************************************************************************************************************/
int globalInputCode = 0;

int globalAge = 0;
int globalSex = 0;
int globalCivilStatus = 0;

typedef enum {
	Success,
	Error,
	ERROR_TERMINAL,
	ERROR_CONVERSION
} Result;


/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
Result CollectData();
bool Calculate();
bool ShowResults();

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

bool Run(){
	
	while (true){
		if (CollectData() == Error){
			cout<<"Error al digitar el codigo, vuelva a ingresar el codigo\r\n";
			continue;
		}
		
		if (Calculate() == false ){
			cout<<"Codigo ingresa es invalido, vuelva a ingresar el codigo\r\n";
			continue;
		}
		
		if (ShowResults() == false ){
			cout<<"Error en Show Results\r\n";
			continue;
		}
		
		break;
	}
			
	return true;
}
//=====================================================================================================

Result CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Calculation in a course\r\n";
	cout<<"\tEnter the code: ";
	cin>>globalInputCode;
	
	// Preguntar si hubo un error al guardar el dato en un variable int
	if (cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		globalInputCode = 0;
		cout<<"Typed code is not valid\r\n";
		return Error;
	}
	
	// Verificar que es code es de 4 digitos
	if((globalInputCode >= 10000) || (globalInputCode < 1000)){
		cout<<"Error length is not valid\r\n";
		return Error;
	}
	
	return Success;	
}
//=====================================================================================================

bool Calculate(){
	globalCivilStatus = (globalInputCode%10000-globalInputCode%1000)/1000;
	globalAge = (globalInputCode%1000-globalInputCode%10)/10;
	globalSex = globalInputCode%10;
	
	// Verifico estado civil
	if (globalCivilStatus > 4){
		cout<<"Civil Status not valid\r\n";
		return false;
	}
	
	// Verifico Edad
	if (globalAge == 0){
		cout<<"Age is 0\r\n";
		return false;
	}
	
	// Verifico Sexo
	if (globalSex > 2 || globalSex == 0){
		cout<<"The gender is invalid\r\n";
		return false;
	}
	
	return true;
}
//=====================================================================================================
bool ShowResults(){
	cout<<"============Show result============\r\n";
	
	switch(globalCivilStatus){
		case 1:
			cout<<"\tSingle"<<"\r\n";	
			break;
		case 2:
			cout<<"\tMarried"<<"\r\n";
			break;
		case 3:
			cout<<"\tWidower"<<"\r\n";
			break;	
		case 4:
			cout<<"\tDivorced"<<"\r\n";
			break;
		default:
			cout<<"INCORRECT CODE"<<"\r\n";
			break;
	}
	   
	if(globalSex == 1){
		cout<<"\tFemenine"<<"\r\n";
	}else if(globalSex == 2){
		cout<<"\tMale"<<"\r\n";
	}else{
		cout<<"INCORRECT CODE"<<"\r\n";
	}
	
	cout<<"\tThe age is: "<<globalAge<<" years old\r\n";	
	
	return true;
}


