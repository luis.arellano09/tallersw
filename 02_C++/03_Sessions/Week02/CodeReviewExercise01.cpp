/**
 * @file CodeReview.cpp
 * @author Luis Arellano (luis.arellano09@gmail.com)
 * @brief Excercise 01:
 		Elabore un algoritmo que dados como datos  de
		entrada el radio y la altura de un cilindro calcular, el rea lateral y el volumen del cilindro.
		A = pi^2radio*altura	V =radio^2pi*altura
 * @version 1.0
 * @date 04.12.2021
 * 
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>

using namespace std;

/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/
#define PI 3.1416		// Constante Pi 

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
			
	// Declaration and Initialization of variables
	float terminalInputHeight = 0.0; 		// Entrada de altura a traves del terminal
	float terminalInputRadius = 0.0;	 	// Entrada de radio a traves del terminal
	float circularArea = 0.0;				// Area circular calculada
	float lateralAreaCylinder = 0.0;		// Area lateral del cilindro calculada
	float volumeCylinder = 0.0;				// Volumen del cilindro calculada
	
	// Ask data throught the terminal
	cout<<"========= Insert Data ===========\r\n";
	cout<<"\tWrite the radius of the cylinder: ";
	cin>>terminalInputRadius;
	cout<<"\tWrite the height of the cylinder: ";
	cin>>terminalInputHeight;
	
	// Calculation
	circularArea = PI * pow(terminalInputRadius,2.0);							// area_circulo = pi^2radio
	lateralAreaCylinder = 2.0 * PI * terminalInputRadius * terminalInputHeight;	// area lateral = 2 * pi * radio * h
	volumeCylinder = circularArea*terminalInputHeight;							// valumen = area_circulo * altura
	
	// Show results in terminal
	cout<<"\r\n========= Show Results ===========\r\n";
	cout<<"\tThe circular area is: "<< circularArea<<"\r\n";
	cout<<"\tThe lateral area is: "<<lateralAreaCylinder<<endl;
	cout<<"\tThe volume is: "<<volumeCylinder<<"\r\n";
	
	return 0;
}




 
