#include <iostream>
 
using namespace std;
 
 /*
 1. Operacion que hace operaciones y puede retorna valores
 2. Reutilizable
 */

// Global Variable
int contadorHombres = 0;
int contadorMujeres = 0;
 
// Declaracion de funciones
void Saludar();
int SumarTresNumeros(int numero1, int numero2, int numero3);
void IncrementarContadores();
void MostrarResultados();
 
int main(){
	
//	Saludar();
// 	Saludar();
// 	Saludar();

//	int pepito = 0;
//
// 	pepito = SumarTresNumeros(5,20,30);
// 	pepito = pepito + 10;
// 	cout<<pepito;

	IncrementarContadores();
	MostrarResultados();
	
 	return 0;
}
 
// Definicion de Funciones
void Saludar(){
	cout<<"Hola Mundo desde la funcion\r\n";
}

int SumarTresNumeros(int numero1, int numero2, int numero3){
	int resultado = 0;
	resultado = numero1 + numero2 + numero3;
	return resultado;
}

void IncrementarContadores(){
	contadorHombres = contadorHombres + 1;
	contadorMujeres = contadorMujeres + 2;
}

void MostrarResultados(){
	cout<<"Hombres = "<<contadorHombres<<"\r\n";
	cout<<"Mujeres = "<<contadorMujeres<<"\r\n";
}

 
