/**
 * @file Condicionales.cpp
 * @author Luis Arellano (luis.arellano09@gmail.com)
 * @brief File description
 * @version 1.0
 * @date 29.01.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;


/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/

void Run();
void CollectData();
void ShowResult();
bool CheckHour(int hour, int minHour, int maxHour);
bool CheckAge(int age, int minAge, int maxAge);
bool CheckSpecialPass(bool specialPass);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	cout<<"Condicionales\r\n";
	
	int edadPersona = 70;
	bool hasSpecialPass = true;
	int currentTimeHour = 3;
	
//	if ( ((edadPersona >= 18 && edadPersona < 65) || hasSpecialPass == true) && ( currentTimeHour > 4 && currentTimeHour < 24) ){
//		cout<<"Ingreso permitido\r\n";
//	} else {
//		cout<<"Ingreso no permitido\r\n";
//	}
	
	
	if ( currentTimeHour > 4 && currentTimeHour < 24) {
		if (edadPersona >= 18 && edadPersona < 65) {
			cout<<"Ingreso permitido\r\n";
			cout<<"Bienvenido\r\n";
		} else {
			if (hasSpecialPass == true) {
				cout<<"Ingreso permitido con Special Pass\r\n";
				cout<<"Bienvenido\r\n";
			} else {
				cout<<"Ingreso no permitido: No cumple con el rango de edad\r\n";
			}
		}
	} else {
		cout<<"Ingreso no permitido: Toque de queda\r\n";
	}
		
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/










