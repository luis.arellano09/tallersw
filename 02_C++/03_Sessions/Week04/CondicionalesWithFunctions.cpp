/**
 * @file Condicionales.cpp
 * @author Luis Arellano (luis.arellano09@gmail.com)
 * @brief File description
 * @version 1.0
 * @date 29.01.2022
 * 
 * && y
 * || o 
 * == igual
 * != diferente
 *
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/

int globalParamMinHour = 6;
int globalParamMaxHour = 23;
int globalParamMinAge = 18;
int globalParamMaxAge = 65;

int globalInputCurrentTimeHour = 0;
int globalInputPersonAge = 0;
bool globalInputHasSpecialPass = false;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/

void Run();
void CollectData();
void CheckAccess();
bool CheckHour(int hour, int minHour, int maxHour);
bool CheckAge(int age, int minAge, int maxAge);
bool CheckSpecialPass(bool specialPass);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	
	Run();
		
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	CheckAccess();
}
//=====================================================================================================

void CollectData(){
	
	cout<<"============Insert data============\r\n";
	
	cout<<"Ingrese la hora: ";
	cin>>globalInputCurrentTimeHour;
	
	cout<<"Ingrese la edad de la persona: ";
	cin>>globalInputPersonAge;

	int tempHasSpecialPass = 0;
	cout<<"Ingrese si cuenta con el Special Pass (No=0, Si=1) ";
	cin>>tempHasSpecialPass;
	
	if ( tempHasSpecialPass == 1) {
		globalInputHasSpecialPass = true;
	} else {
		globalInputHasSpecialPass = false;
	}
}
//=====================================================================================================

void CheckAccess(){
	if ( CheckHour(globalInputCurrentTimeHour,globalParamMinHour,globalParamMaxHour) ) {
		if ( CheckAge(globalInputPersonAge,globalParamMinAge,globalParamMaxAge) ) {
			cout<<"Ingreso permitido\r\n";
			cout<<"Bienvenido\r\n";
		} else {
			if ( CheckSpecialPass(globalInputHasSpecialPass) ) {
				cout<<"Ingreso permitido con Special Pass\r\n";
				cout<<"Bienvenido\r\n";
			} else {
				cout<<"Ingreso no permitido: No cumple con el rango de edad\r\n";
			}
		}
	} else {
		cout<<"Ingreso no permitido: Toque de queda\r\n";
	}	
}
//=====================================================================================================

bool CheckHour(int hour, int minHour, int maxHour){
	if ( hour >= minHour && hour <= maxHour) {
		return true;
	} else {
		return false;
	}
}
//=====================================================================================================

bool CheckAge(int age, int minAge, int maxAge){
	if ( age >= minAge && age <= maxAge) {
		return true;
	} else {
		return false;
	}
}
//=====================================================================================================

bool CheckSpecialPass(bool specialPass){
	return specialPass;
}
//=====================================================================================================













